+++
title = "Compendium"
description = "Compendium of Foes for the HundredHack system"
weight = 300
+++

The compendium of foes for Hundredhack is made up of several categories:

{{% children style="h5" description="true" depth="1" sort="weight" %}}