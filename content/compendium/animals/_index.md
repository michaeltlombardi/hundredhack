+++
title = "Animals"
description = "A list of basic animals compatible with the core HundredHack rules"
weight = 100
+++

Animals are non-sentient creatures of the world.
Like [characters](../../rules/example-characters), animals have characteristics, attributes, knacks, and vocations.

Unlike characters, however, _wild_ animals do not have skills - though domesticated animals can be taught skills by their sentient companions.
Instead, an animal uses their [_vocation_](../../rules/optional/vocations) to determine their success at skills which you might expect that animal to be good at - for example, a bear would get their vocation bonus for hunting or for wrestling, but not for finding their way around a mansion or for climbing a wall.

The animals listed in this section are archetypal, a group of common, basic animals which referees can reskin or hack into a vast multitude of other animals.
As with all other sections of these rules, the animals presented here are meant to be hacked on to fit your setting, table, and session.

{{% animals include="core" %}}