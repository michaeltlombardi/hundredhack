+++
title = "Credits"
description = "Contributors and packages used by HundredHack"
+++


## GitLab Contributors
{{%glcontributors "https://gitlab.com/api/v4/projects/5261956/repository/contributors" %}}

### Other Contribution
<div class="ghContributors">

<div>
<img src="https://secure.gravatar.com/avatar/fc46b0a966194f78951002fff4b175d9?s=180&d=identicon" class="inline" width="32" height="32" style="height: 32px;height: 32px;margin-bottom:.25em; vertical-align:middle; ">
<label>@<a href="https://gitlab.com/RomanH">Roman Hechenberger</a></label>
<span class="contributions">Editing and Advice</span>
</div>

</div>



## Packages and libraries
* [Bootstrap](http://getbootstrap.com) - front-end framework
* [mermaid](https://knsv.github.io/mermaid) - generation of diagram and flowchart from text in a similar manner as markdown
* [font awesome](http://fontawesome.io/) - the iconic font and CSS framework
* [jQuery](https://jquery.com) - The Write Less, Do More, JavaScript Library
* [lunr](https://lunrjs.com) - Lunr enables you to provide a great search experience without the need for external, server-side, search services...
* [JavaScript-autoComplete](https://github.com/Pixabay/JavaScript-autoComplete) - An extremely lightweight and powerful vanilla JavaScript completion suggester.
* [clipboard.js](https://zenorocha.github.io/clipboard.js) - copy text to clipboard
* [highlight.js](https://highlightjs.org) - Javascript syntax highlighter
* [modernizr](https://modernizr.com) - A JavaScript toolkit that allows web developers to use new CSS3 and HTML5 features while maintaining a fine level of control over browsers that don't support
* [reveal-js](http://lab.hakim.se/reveal-js) - The HTML Presentation Framework

## Tooling

* [Netlify](https://www.netlify.com) - Continuous deployement and hosting of this documentation
* [Hugo](https://gohugo.io/) {{%icon "fa-smile-o"%}}