+++
title = "HundredHack Rules"
description = "Rules for the HundredHack system"
weight = 100
+++

The rules for HundredHack are broken into three categories:

{{% children style="h5" description="true" depth="1" sort="weight" %}}