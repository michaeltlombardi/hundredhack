+++
title = "Optional Rules"
description = "Optional rules and how they fit into the 100H system"
weight = 20
+++

## Using Optional Rules

HundredHack is a system designed with the DIY ethos firmly in mind; for this reason, very few rules are included in the box.
The intention is to provide numerous modular rules that you can combine to make a game that fits your group and style.

Below is the list of optional rules we've for adding to your HundredHack game - if you write your own rules, please let us know and we'll do our best to get them included here:

{{% children style="card" description="true" depth="3" %}}
