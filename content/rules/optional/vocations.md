+++
title = "Vocations"
description = "Optional mechanics for character vocations in HundredHack."
+++

A vocation is a calling or an occupation to which a character is inexorably drawn and which helps to define them.
Vocations, like skills, have a percentage bonus, and can be improved by use and by spending IP, just as skills can.

A vocation is used any time the test at hand would be _directly_ in the domain of the vocation without any penalty.
If the test at hand is _tangentially_ related to the vocation (for example, a blacksmith attempting to pick a lock), they may still add their vocation bonus but the test is one step harder.

Every character begins play with a single vocation at 25% and can optionally acquire a second vocation later by spending five IP.
No character can have more than two vocations at a single time.
If a character wants to trade their vocation for another, they can do so freely - though their vocation score drops by half.

{{% example "Vocations in Play" %}}
At the start of play, Karacter selects Guardian as their vocation - Karacter feels a calling to protect the innocent and the less fortunate and to put their life on the line for the good of the realm.

Anytime it is reasonable that a test applies directly to this vocation - when convincing bandits not to attack, or helping to hide folk from their pursuers, or investigating a murder, or fighting to protect an injured comrade, etc, Karacter can apply their vocation bonus to those tests as normal.

If Karacter wanted to convince an outnumbered enemy to lay down arms, or to bribe a priest into looking the other way from a shipment of weapons, etc, Karacter could still apply their vocation bonus, but only by making the test one step harder.
{{% /example %}}
