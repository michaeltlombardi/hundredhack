+++
title = "Poison"
description = "Optional mechanics for and examples of poisons in HundredHack."
+++

Plants and creatures have developed poisons as a method of protecting themselves against predators.
They are also used by assassins and wrong doers of all kinds to murder their victims.

Every type of poison has the following information detailed:

{{% poison _explanation %}}

Poisoned characters make an opposed test against the poison's potency:

+ **Poison Succeeds, Character Fails:** If the poison succeeds its Potency test and the character fails his Resilience test, the poison has its full effect.
+ **Character Succeeds, Poison Fails:** If the character succeeds his Resilience test and the poison fails its Potency test, the poison has no effect.
+ **Both Poison and Character Succeed:** Whoever rolled the highest in their test wins.
+ **Both Poison and Character Fail:** Whoever rolled the lowest in their test wins.

## Poisons List

The following list of poisons is _non-exhaustive_.

{{% poisons linkify="true" include="core" %}}
