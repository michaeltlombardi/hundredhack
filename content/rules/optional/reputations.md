+++
title = "Reputations"
description = "Optional mechanics for character reputations in HundredHack."
+++

Reputations are statements or adjectives about characters that _other people_ may believe.
For example, a character might have a reputation for being unlucky, or frightful, or earnest, or for being a priest-killer, or for being timid, or any number of other possibilities.

Reputations function as a special type of skill and have a percentage bonus.
Whenever interacting with someone who might know the character's reputation, and in the first situation where that reputation might matter, make a POW test, adding the specific reputation bonus.
If successful, the person the character is interacting with believes in the character's reputation.
If unsuccessful, the person does _not_ believe in the reputation.
In either case, do not test for that reputation again with this person unless the reputation bonus changes.

In general, reputations can make social tests either one step easier or more difficult, depending on the reputation and the context.
In some cases, a reputation may even make a test unnecessary.

Characters begin play with two reputations of their choice, both starting at 25%.

Every time a character performs an action where the action can be observed or recorded and the action could either reinforce or disabuse observers of the reputation, place a mark to the left (if the action could reinforce the reputation) or the right (if the action could disabuse observers of the reputation).
At the completion of a quest or when beginning an interlude - whenever there's a sensible break in the quest to reflect and take time to recover - players should roll 1D100 for each mark beside their reputations.

For each mark to the left of the reputation, roll 1D100.
If the result of the roll is higher than the reputation, increase the reputation by 3.

For each mark to the right of the reputation, roll 1D100.
If the result of the roll is lower than the reputation, decrease the reputation by 3.

Characters can also spend IP to improve or reduce a reputation, improving or reducing it by 5% for each IP spent.
Characters can have any number of different reputations.

{{% example "Reputations in Play" %}}
At the beginning of play we select two reputations for Karacter, both of which will start at 25%.
We'll choose to give Karacter a reputation for being _earnest_ and another for being _simple_ - people think of Karacter as being basically decent and dependable, if not the most articulate or clever person.

During play, Karacter wants to get past the front clerk at a business to speak with the proprietor, who is in her office, to convince the proprietor that Karacter should be hired on as a warehouse guard.
We decide that being earnest would be useful for landing the job, and so the referee calls for a hard reputation test - it is very unlikely that the front clerk has heard of Karacter at this point.

With a POW of 13, Karacter's base goal is 39%, to which we add their reputation bonus (25%).
Unfortunately, the test is going to be hard, so we cut the total goal in half - Karacter must roll under 32%.

The roll is a 33, meaning that the front clerk doesn't believe in Karacter's reputation for being earnest - note that this does not mean that the clerk automatically thinks Karacter is untrustworthy either.

We still want to convince the clerk to let us speak with the proprietor, so Karacter will try to convince the clerk that they are serious about the job opening and a good fit for it.
The referee calls for a POW test and Karacter adds their leadership skill bonus (50%) to the test,
Karacter rolls an 86 which is under their 89% goal.
The referee rules that the clerk is swayed by Karacter's short speech, appearance, and general presence, and walks Karacter to the back office, introducing Karacter to the proprietor.

Again, we make a test to see if the proprietor believes in Karacter's reputation.
This time however, the test is not hard - the proprietor is better connected than the clerk and keeps in contact with several mercenary groups in the city.

The result is a 38 and the proprietor _does_ in fact believe that Karacter is an earnest young would-be warrior.
The test to convince the proprietor to hire Karacter will be one step easier.

Later, when Karacter turns in a coworker to the proprietor for stealing from the supplies whilst on duty, we place a mark to the left of the _earnest_ reputation - this act will potentially improve Karacter's reputation.
{{% /example %}}