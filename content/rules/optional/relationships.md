+++
title = "Relationships"
description = "Optional mechanics for relationships in HundredHack."
+++

When you begin play, choose an ally, a dependent, and either an enemy or an organization.
You have a relationship bonus with each of these equal to 25%.

+ **Ally**. This is a group or person a player character can ask for aid, can call in favors from, or with whom they otherwise have a positive relationship.
+ **Dependent**. This is usually the character's family or a group that relies upon them.
+ **Enemy**. This is a sworn enemy.
  Interactions with this person or group are usually negative.
  They may be a recurring foe or ethnic enemy.
+ **Organization**.This could be a wizards' sorority, a former regiment or even a cult.
  The character has an association with this organization; they typically deal with this group on a favorable basis.
  GMs should determine the maximum scale of an organization, which could be as great as a whole nation, or just a small wandering band of minstrels.

On a critical success for a relationship test, the character gains a very positive response, and an increase of 1D6% to their relationship.
On a fumble the character damages their relationship with the party and loses 1D4% from their skill.
Further, after a fumble, the player cannot call on a relationship for rest of the adventure.
In the case of enemies this means that they have accidentally aided them in some way.

| Relationship | IP Cost | Advantages (must make a successful relationship roll) | Disadvantages |
|:------------:|:-------:|:------------------------------------------------------|:--------------|
| Ally         | 2       | Loan an appropriate item or finances; assist on social test; provide info and/or employment | Allies demand favors of the character, in the form of money, errands etc for each favor given; enemies of your ally become your enemies.
| Dependent    | 1       | Characters gain 1-3 additional Improvement Points where they support, aid or defend Dependents. | Dependents regularly get themselves and the character into trouble; Enemies will target dependents to harm the character
| Enemy        | 2       | Gains +25% to tests in the attempt to thwart their enemy;  gain 1-3 additional IP for thwarting an enemy's plans; gain employment by enemy's opponents | Enemies will try to harm characters as often as possible; All interactions (parleys, influence, negotiation) with an enemy will be at one difficulty rating harder than normal.
| Organization | 2       | Can access the facilities offered by the organization; can gain information and possible employment | Organizations typically demand a charge for their services, either in cash or a favor; Enemies of an organization may harry and harm the character for their relationship to the organization.

You can purchase new relationships later with improvement points for the cost listed; new relationships start at a bonus of 10%.

{{% example "Relationships in Play" %}}
At the start of play, Karacter selects Warden, their aunt, who as an ally.
Karacter takes Spot, a miniature tiger, as their dependent.
Finally, as Karacter's goal is to become a full fledged member of the Wardens, Karacter takes them as an organizational relationship.

During Karacter's first session it becomes clear that lacking a proper weapon (Karacter only has a single dagger which doubles as their utility knife) is a problem.
Karacter asks their aunt, Warden, for a loaner weapon and makes a relationship test.
Karacter's POW is 11, making the base goal 33% - Karacter adds their relationship bonus, 25%, for a total goal of 58%.

Karacter rolls a 32 and the referee declares that Aunt Warden lends an old shortsword and a spare longbow to Karacter - on the condition that Karacter must succeed in the trials to join the Wardens or return the weapons upon failure.

Later in the adventure, Karacter is ambushed in an alley by Spy, who has been paid by the father of a man Warden killed in a judicial duel - Karacter has inherited their aunt's enemies.
{{% /example %}}
