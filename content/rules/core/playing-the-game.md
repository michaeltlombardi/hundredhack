+++
title = "Playing the Game"
description = "Miscellaneous rules for playing 100H."
weight = 140
+++

## Time and Turns

There are two types of tracked time in 100H - moments and minutes.
Moments are used during combat and fast paced scenes of danger and minutes are used when exploring and adventuring.
A referee may advance the clock as they need substituting minutes for hours, days, or even months, should the adventure require it.

Roughly speaking, a moment is equal to five seconds of time.

## Movement and Distance

Rather than track precise numbers, 100H uses 4 abstract ranges for measuring distances: close, nearby, far-away and distant.
On their turn every character can move somewhere nearby as part of an action, performing that action at any stage of the move.
They can forgo their action and move somewhere far-away instead.
Anything beyond far-away can be classified as distant and would take 3 moves to get to.

This system is designed to support the narrative 'theater of the mind' style of play, and is less concerned about tracking squares and distances.
For converting existing movement rates or measures (for spells or areas of effect) use the following as a guide:

|  Close  |  Nearby  |  Far Away  |
|:-------:|:--------:|:----------:|
| 0 - 5ft | 5 - 60ft | 60 - 120ft |

## Initiative

When combat breaks out, everyone must be sorted into an order so they each get to act and react in turn.
Every character tests their INT.

The characters that fail declare their actions first, from lowest to highest roll.
Then characters that succeed declare their actions, from lowest to highest roll.
Finally, characters that critically succeed, from lowest to highest.

Once all actions are declared, they are resolved in reverse order.

{{% example "Initiative in a Moment" %}}
In the local pub, a fight breaks out between Karacter, a Warden, and three drunks.
Karacter has a base INT goal of 27%, to which they add their highest relevant skill bonus (50%, leadership) and tests against 77%.
The Warden, being very experienced, has a goal of 86%, though her inebriation makes the test hard, reducing her goal to 43%.
Likewise, the three drunks have initiative goals of 25%.

Everyone rolls:

+ Karacter rolls a 92
+ The warden rolls a 13
+ Drunk A (henceforth, A) rolls a 21
+ Drunk B (henceforth, B) rolls a 14
+ Drunk C (henceforth, C) rolls a 99

The warden, A, and B succeed on their initiative test, though C and Karacter fail.

Karacter's roll was lower than C's, so Karacter declares first, followed by C.
Warden rolled the lowest of those who succeeded and so declares next, followed by B and finally by A.

1. Karacter declares that they will try to restrain C, as C is the drunkest of their opponents.
2. C declares that they will back away and grab a bottle to use as a club against Karacter when Karacter comes in to grapple.
3. Warden, seeing that Karacter is in a bit of trouble, declares that she will move up beside Karacter and defend against A and B.
4. B declares that he will tackle Warden.
5. A declares that he will throw a bottle at Warden to put him off balance.

Those actions are then resolved in reverse, starting with A and finishing with Karacter.

{{% /example %}}

## Player's Turn

During a player’s turn a character may move, perform an action, and (if appropriate) perform a reaction.
Example actions include (but are _not_ limited to) attacking an enemy, looking for a clue, convincing an NPC, exercising a knack, drinking a potion, and pulling a lever.

## Attacking, Defending, and Damage

When a character attacks a creature they must roll below their BOD score for both melee and ranged attacks.

Characters have one reaction they can use in each moment, typically reacting to an incoming attack by making an opposed test against the attack to parry or dodge. triggering an opposed test to avoid or reduce the damage from the incoming attack.
As with actions, reactions are declared during initiative order, so characters can only react to attacks declared before their action.

## Major Wounds

Any time a creature or character takes damage greater than or equal to half their total HP from a single source (one attack, knack, trap, etc) in one moment then the injured creature or character suffers a major wound.
The referee rolls on the table below to determine the impact of this major wound.

| Result on 1D10 | Major Wound |
|:--------------:|:------------|
|        1       | _Lose an eye_. All visual Perception tests are now one step harder, lose 2 points of BOD, 1 point of POW permanently.
|        2       | _Cracked skull_. Lose 4 points of INT. All skills involving mental processes become -25 permanently.
|        3       | _Left Leg crippled_. Fall prone, move half speed. Lose 2 points of BOD permanently.
|        4       | _Right Leg crippled_. Fall prone, move half speed. Lose 2 points of BOD permanently.
|        5       | _Broken ribs_. All skills are one step harder, due to severe pain.
|        6       | _Slashed stomach_. Lose one extra HP per moment from blood loss. Lose 3 points of BOD permanently.
|        7       | _Heart stops in shock!_ Unconscious for D10 moments, fall prone and can not move. Lose 2 points of BOD permanently.
|        8       | _Spine broken_. Permanently paralyzed below the torso. Half BOD permanently.
|        9       | _Left Arm crippled_. Automatically drop any held items.
|        10      | _Right Arm crippled_. Automatically drop any held items.

## Dying

When a character is reduced to zero HP they are dead.

## Healing

Characters can regain HP through potions, knacks, and by resting.
They can never regain more than their maximum HP.

## Resting

Once per day, after resting, characters regain all expended PP.
Additionally, characters may make a BOD test to regain HP - if successful, they regain 1D4 + 1 HP.
If unsuccessful, they regain only 1HP.

## Fatigue

Combat, sprinting, climbing, swimming against a strong current, are all examples of when a character can become fatigued and tired.

If the Games Master thinks that a character has been engaged in an activity that may have drained him of physical energy, then they may call for a fatigue.
If the character fails the roll they suffer the effects of fatigue, progressing through the following cumulative levels of fatigue with each successive failed fatigue test:

1. Further tests to resist fatigue are one step harder.
2. All athletics tests are now one step harder as well.
3. All physical tests are one step harder.
4. All tests are one step harder.
5. Character is unconscious for 3D6 minutes and is still at four levels of fatigue upon waking.

{{% example "Fatigue" %}}
After a pub fight, the referee calls for a fatigue test for Karacter.
With a BOD of 13, Karacter's base goal is 39% - Karacter's player makes the argument that their wrestling skill (30%) should apply to this test because much of the training for wrestling involves endurance conditioning.
The referee agrees and Karacter still fails the test with a result of 70%.

The referee declares that Karacter is now suffering from one degree of fatigue - future tests to resist fatigue will now be hard for Karacter.
{{% /example %}}

### Recovering From Fatigue

A character who rests for 24 - BOD hours will remove the effects of any fatigue.
One use of the vigor knack will also remove the effects of fatigue.
