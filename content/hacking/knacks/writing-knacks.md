+++
title = "Writing Knacks"
description = "Guidance for writing your own knacks in 100H"
weight = 30
+++

In the previous section we covered how to hack up and use existing knacks.
But what if nothing in the knacks list available to you fits _exactly_ what you want?
What if you have a concept that isn't even remotely addressed by an existing knack?

That's when it's time to write up a new knack.
We're going to write a new knack for this document, something not covered in the core knacks.
Let's figure out how to give a character wings.

Keep in mind all the lessons from the previous section on knacks about explaining them, gating them, combining them, hacking them, and narrowing them.
You can do all of those things when writing up brand new knacks, too.

But the first thing to do when writing up a new knack (after coming up with a concept, of course) is to think about the traits and, specifically, whether it deviates from the standard ones.

{{% excerpt-include filename="rules/core/knacks.md" panel="From the Knacks Core Rules:" %}}

So, when looking over these traits we have an immediate choice to make: are the wings always there, or do they grow and shed at the end of the duration?
Both options can produce some interesting outcomes, but for the purposes of this example let's say the wings should be passive and permanent, an always-there feature of any character with this knack.
The range should also be changed to self since it's about the character with the knack having wings, not granting wings to other people.

Most of the other traits don't make sense if the wings are passive and permanent- they can't be triggered, don't have a magnitude-dependent duration, don't require concentration, don't have an area of effect or a range, can't be resisted, etc.
A few of the traits still need to be worked out though - should the base magnitude be higher than 1?
Should the knack itself be non-variable or should increased magnitude have a better effect?

Let's bump the base magnitude to three but _not_ make the knack non-variable, allowing the character to spend more points to get better wings.

Now our knack's traits look like this:

+ Magnitude 3, Passive, Permanent, Range (Self)

Great! But now we need to define the actual effect.

> A character with this knack grows a pair of wings which allow them to glide during their movement.
> For an additional three points of magnitude the character can fly in any direction they choose in addition to being able to glide.

This establishes that a character taking this knack initially can only glide, not actually fly, unless they invest an additional 3 points of magnitude into the knack.

We now have a fairly good basis for a [wings knack](#knack-wings-basic)

{{% knack wings-basic %}}

But it's kinda weird that anyone can just grow wings, right?
Let's add some setting specific changes, assuming an urban fantasy setting:

{{% knack wings-urban-fantasy %}}

Here we specify that the knack requires that the character meet specific narrative requirements - they need to have some sensible reason to be born with wings - and also the stipulation that wings must be chosen at character generation and cannot be acquired later in the game.

So now we have a new knack for our game that characters can choose to buy at character generation.

But let's go ahead and hack it so there's a version available to characters _after_ generation:

{{% knack wings-post-gen %}}

We've made it so the wings now have a duration and changed their cost.
They require a relationship with a demon or angel who can grant this profane/divine gift and the wings burn off at the end of the duration.

## Considerations

When writing knacks, look towards existing knacks, especially those in core.
Consider whether you can hack up existing knacks to suit you - and if not, use those existing knacks to help guide you while making a new one.

Very few knacks have a starting magnitude (or a non-variable one) of more than 5.
If you're adding new passive knacks consider gating them or under-powering them compared to pay-per-use knacks.

Be mindful that knacks which make someone more effective in combat should probably be a higher magnitude, or narrower, or gated, or have drawbacks - choosing between knacks should always call for interesting trade-offs rather than outright 'best' options.

Always keep in mind the all important _feel_ of a knack - very often you'll know a poor-fitting knack without being able to precisely articulate why it doesn't fit - err on the side of trusting that instinct.
But also don't forget that it's _good_ to experiment and fix things.
Your players will be forgiving, especially if you're up front with them about it - and players who are getting specially hacked-up abilities to suit their characters are likely to overlook experimental missteps.
